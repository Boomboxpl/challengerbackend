from rest_framework import serializers

from games.models import Game
from games.serializers import GameSerializer
from .models import HighScore, SubScore
from drf_writable_nested import WritableNestedModelSerializer

class HighScoreSerializer(serializers.ModelSerializer):
    tiding = serializers.PrimaryKeyRelatedField(read_only = True)
    game = GameSerializer()

    class Meta:
        model = HighScore 
        fields = '__all__'
        
        
class SubScoreSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = SubScore
        fields = ['pk', 'priority', 'name', 'values']
        
class HighScoreCreateSerializer(WritableNestedModelSerializer, serializers.ModelSerializer):
    subscore_set = SubScoreSerializer(many = True)

    class Meta:
        model = HighScore
        exclude = ['user']
    
class HighScoreFullSerializer(HighScoreSerializer):
    subscore_set = SubScoreSerializer(many = True)

    