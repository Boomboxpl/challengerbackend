
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from games.permissions import GameApiPermission
from .models import HighScore
from .serializers import (HighScoreCreateSerializer, HighScoreSerializer, HighScoreFullSerializer)
from challenger.pagination import CursorCreatedPagination
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from challenger.permissions import OwnerPermission
from drf_spectacular.utils import OpenApiParameter, extend_schema, extend_schema_field
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

# Create your views here.

@extend_schema( 
               parameters= 
               [OpenApiParameter( 
                     name='GAME-API', 
                     type=str, 
                     description = 'Security key for POST highscores only from managed games',
                     location=OpenApiParameter.HEADER, 
                     response=False, 
                 )])
class HighScoreCreate(generics.CreateAPIView):
    permission_classes = [IsAuthenticated, OwnerPermission, GameApiPermission]
    serializer_class = HighScoreCreateSerializer
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class HighScoreDelete(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated, OwnerPermission) 
    serializer_class = HighScoreSerializer
    
class HighScoreUpdate(generics.UpdateAPIView):
    permission_classes = (IsAuthenticated, OwnerPermission) 
    serializer_class = HighScoreSerializer

class UserHighScores(generics.ListAPIView):
    lookup_field = 'user'
    lookup_url_kwarg = 'userId'
    serializer_class = HighScoreSerializer
    pagination_class = CursorCreatedPagination
    filterset_fields = {
        'created' : ['range'],
    }
    ordering_fields = '__all__'
    ordering = ['-created']
    
    def get_queryset(self):
        return HighScore.objects.exclude(user__isnull=True).filter(user__id=self.kwargs[self.lookup_url_kwarg])
    
    @method_decorator(cache_page(30))
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)

class HighScoreList(generics.ListAPIView):
    queryset = HighScore.objects.all()
    serializer_class = HighScoreSerializer
    pagination_class = CursorCreatedPagination
    
    filterset_fields = {
        'created' : ['range'],
        'name' : ['exact', 'contains'],
        'user' : ['exact'],
        'game' : ['exact'],
    }
    ordering_fields = '__all__'
    ordering = ['-created']
    
    @method_decorator(cache_page(30))
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    
class HighScoreDetail(generics.RetrieveAPIView):
    queryset = HighScore.objects.all()
    serializer_class = HighScoreFullSerializer
    
