# Generated by Django 4.1.3 on 2022-11-13 13:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('highscores', '0005_remove_subscoreweight_parent_remove_subscore_value_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subscore',
            options={'ordering': ['parent', '-priority', 'pk']},
        ),
    ]
