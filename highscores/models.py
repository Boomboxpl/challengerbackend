from django.contrib.auth.models import User
from django.db import models

from games.models import Game

# Create your models here.

class HighScore(models.Model):
    name = models.CharField("Name", max_length=240)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null= True)
    game = models.ForeignKey(Game, on_delete=models.SET_NULL, null= True)
    created = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name
    
class SubScore(models.Model):
    parent = models.ForeignKey(HighScore, on_delete=models.CASCADE)
    name = models.CharField("Name", max_length=240)
    priority = models.IntegerField(default= -1)
    values = models.JSONField(null = True, blank = True)
    
    class Meta:
        ordering = ['parent', '-priority', 'pk']