from django.urls import path
from .views import HighScoreCreate, HighScoreList, HighScoreDetail, UserHighScores, HighScoreDelete, HighScoreUpdate


urlpatterns = [
    path('', HighScoreList.as_view()),
    path('user/<int:userId>', UserHighScores.as_view()),
    path('<int:pk>/', HighScoreDetail.as_view(), name='retrieve-highscore'),
    path('create/', HighScoreCreate.as_view(), name='create-highscore'),
    path('update/<int:pk>/', HighScoreUpdate.as_view(), name='update-highscore'),
    path('delete/<int:pk>/', HighScoreDelete.as_view(), name='delete-highscore'),
]