from rest_framework import serializers
from .models import Profile
from django.contrib.auth import get_user_model
from drf_writable_nested import WritableNestedModelSerializer
from django.core.exceptions import ValidationError
import django.contrib.auth.password_validation as validators
from rest_framework.validators import UniqueValidator

UserModel = get_user_model()

class UserCreateSerializer(serializers.ModelSerializer):
    
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):

        user = UserModel.objects.create_user(
            username=validated_data['username'],
            email = validated_data['email'],
            password=validated_data['password'],
        )

        return user
    
    def validate(self, data):
         user = UserModel(**data)
         password = data.get('password')
         if password is None:
             return super().validate(data)
         
         errors = dict() 
         try:
             validators.validate_password(password=password, user=user)
         except ValidationError as e:
             errors['password'] = list(e.messages)
         
         if errors:
             raise serializers.ValidationError(errors)
         return super().validate(data)

    class Meta:
        model = UserModel
        fields = ( "id", "username", "password", "email")
        
class UserDetailSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=False)
    password = serializers.CharField(write_only=True, required = False)
    
    def validate(self, data):
         user = UserModel(**data)
         
         errors = dict() 
         
         password = data.get('password')
         if not password is None:
            try:
                validators.validate_password(password=password, user=user)
            except ValidationError as e:
                errors['password'] = list(e.messages)
         
         if errors:
             raise serializers.ValidationError(errors)
         return super().validate(data)

    class Meta:
        model = UserModel
        fields = ( "id", "username", "password", "email", "is_staff", "is_active", "is_superuser")
        read_only_fields = ["is_staff", "is_active", "is_superuser"]
        
class UserAdminSerializer(UserDetailSerializer):
    
    class Meta:
        model = UserModel
        fields = '__all__'
   
def UpdateUserData(user, validated_data):
    try:
        userData = validated_data.pop('user')
        if not userData is None:
            for attr, value in userData.items():
                if attr == 'password':
                    user.set_password(value)
                elif attr == 'username':
                    if value != user.username:
                        if UserModel.objects.filter(username = value).exclude(id = user.id).exists():
                            raise serializers.ValidationError({'username': 'A user with that username already exists.'})
                        setattr(user, attr, value) 
                else:
                    setattr(user, attr, value)      
            user.save()     
    except KeyError:
        pass
        
class ProfileSerializer(serializers.ModelSerializer):
    user = UserDetailSerializer(read_only = False, required = False)
    
    def update(self, instance, validated_data):
        UpdateUserData(instance.user, validated_data)
        return super().update(instance, validated_data)

    class Meta:
        model = Profile 
        fields = ['user', 'avatar', 'bio']
        read_only_fields = ['id']
        
class ProfileSerializerAdmin(WritableNestedModelSerializer, serializers.ModelSerializer):
    user = UserAdminSerializer(read_only = False, required = False)

    def update(self, instance, validated_data):
        UpdateUserData(instance.user, validated_data)
        return instance

    class Meta:
        model = Profile 
        fields = '__all__'
        read_only_fields = ['id']
