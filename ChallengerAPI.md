---
title: Challenger API v1.0.2137
language_tabs:
  - python: Python
language_clients:
  - python: ""
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<!-- Generator: Widdershins v4.0.1 -->

<h1 id="challenger-api">Challenger API v1.0.2137</h1>

> Scroll down for code samples, example requests and responses. Select a language for code samples from the tabs above or the mobile navigation menu.

Challenger API for PPP, OIRPOS, PAI Subject. Authors: Filip Gaida, Łukasz Kubicki, Paweł Woch

# Authentication

- HTTP Authentication, scheme: bearer 

<h1 id="challenger-api-authorization">authorization</h1>

## authorization_token_create

<a id="opIdauthorization_token_create"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

r = requests.post('/api/authorization/token/', headers = headers)

print(r.json())

```

`POST /api/authorization/token/`

Takes a set of user credentials and returns an access and refresh JSON web
token pair to prove the authentication of those credentials.

> Body parameter

```json
{
  "username": "string",
  "password": "string"
}
```

```yaml
username: string
password: string

```

<h3 id="authorization_token_create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[TokenObtainPair](#schematokenobtainpair)|true|none|

> Example responses

> 200 Response

```json
{
  "access": "string",
  "refresh": "string"
}
```

<h3 id="authorization_token_create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[TokenObtainPair](#schematokenobtainpair)|

<aside class="success">
This operation does not require authentication
</aside>

## authorization_token_refresh_create

<a id="opIdauthorization_token_refresh_create"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

r = requests.post('/api/authorization/token/refresh/', headers = headers)

print(r.json())

```

`POST /api/authorization/token/refresh/`

Takes a refresh type JSON web token and returns an access type JSON web
token if the refresh token is valid.

> Body parameter

```json
{
  "refresh": "string"
}
```

```yaml
refresh: string

```

<h3 id="authorization_token_refresh_create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[TokenRefresh](#schematokenrefresh)|true|none|

> Example responses

> 200 Response

```json
{
  "access": "string",
  "refresh": "string"
}
```

<h3 id="authorization_token_refresh_create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[TokenRefresh](#schematokenrefresh)|

<aside class="success">
This operation does not require authentication
</aside>

<h1 id="challenger-api-games">games</h1>

## games_list

<a id="opIdgames_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/games/', headers = headers)

print(r.json())

```

`GET /api/games/`

<h3 id="games_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "user": 0
    }
  ]
}
```

<h3 id="games_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedGameList](#schemapaginatedgamelist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## games_retrieve

<a id="opIdgames_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/games/{id}/', headers = headers)

print(r.json())

```

`GET /api/games/{id}/`

<h3 id="games_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "user": 0
}
```

<h3 id="games_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[Game](#schemagame)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## games_create_create

<a id="opIdgames_create_create"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.post('/api/games/create/', headers = headers)

print(r.json())

```

`POST /api/games/create/`

> Body parameter

```json
{
  "name": "string",
  "description": "string",
  "metaData": "string",
  "releaseDate": "2019-08-24"
}
```

```yaml
name: string
description: string
metaData: string
releaseDate: 2019-08-24

```

<h3 id="games_create_create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[GameCreate](#schemagamecreate)|true|none|

> Example responses

> 201 Response

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}
```

<h3 id="games_create_create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|none|[GameCreate](#schemagamecreate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## games_delete_retrieve

<a id="opIdgames_delete_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/games/delete/{id}/', headers = headers)

print(r.json())

```

`GET /api/games/delete/{id}/`

<h3 id="games_delete_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}
```

<h3 id="games_delete_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[GameDeveloper](#schemagamedeveloper)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## games_delete_destroy

<a id="opIdgames_delete_destroy"></a>

> Code samples

```python
import requests
headers = {
  'Authorization': 'Bearer {access-token}'
}

r = requests.delete('/api/games/delete/{id}/', headers = headers)

print(r.json())

```

`DELETE /api/games/delete/{id}/`

<h3 id="games_delete_destroy-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

<h3 id="games_delete_destroy-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|No response body|None|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## games_igdb_retrieve

<a id="opIdgames_igdb_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/games/igdb/{query}', headers = headers)

print(r.json())

```

`GET /api/games/igdb/{query}`

Check request & responses on https://api-docs.igdb.com/#examples

<h3 id="games_igdb_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|query|path|string|true|none|

<h3 id="games_igdb_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|success|None|
|503|[Service Unavailable](https://tools.ietf.org/html/rfc7231#section-6.6.4)|service_unavailable|None|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## games_managed_list

<a id="opIdgames_managed_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/games/managed/', headers = headers)

print(r.json())

```

`GET /api/games/managed/`

<h3 id="games_managed_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "apiKey": "string",
      "user": 0
    }
  ]
}
```

<h3 id="games_managed_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedGameDeveloperList](#schemapaginatedgamedeveloperlist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## games_update_retrieve

<a id="opIdgames_update_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/games/update/{id}/', headers = headers)

print(r.json())

```

`GET /api/games/update/{id}/`

<h3 id="games_update_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}
```

<h3 id="games_update_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[GameDeveloper](#schemagamedeveloper)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## games_update_update

<a id="opIdgames_update_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/games/update/{id}/', headers = headers)

print(r.json())

```

`PUT /api/games/update/{id}/`

> Body parameter

```json
{
  "name": "string",
  "description": "string",
  "metaData": "string",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}
```

```yaml
name: string
description: string
metaData: string
releaseDate: 2019-08-24
apiKey: string
user: 0

```

<h3 id="games_update_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[GameDeveloper](#schemagamedeveloper)|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}
```

<h3 id="games_update_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[GameDeveloper](#schemagamedeveloper)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## games_update_partial_update

<a id="opIdgames_update_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/games/update/{id}/', headers = headers)

print(r.json())

```

`PATCH /api/games/update/{id}/`

> Body parameter

```json
{
  "name": "string",
  "description": "string",
  "metaData": "string",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}
```

```yaml
name: string
description: string
metaData: string
releaseDate: 2019-08-24
apiKey: string
user: 0

```

<h3 id="games_update_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[PatchedGameDeveloper](#schemapatchedgamedeveloper)|false|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}
```

<h3 id="games_update_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[GameDeveloper](#schemagamedeveloper)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

<h1 id="challenger-api-highscores">highscores</h1>

## highscores_list

<a id="opIdhighscores_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/highscores/', headers = headers)

print(r.json())

```

`GET /api/highscores/`

<h3 id="highscores_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|created__range|query|array[string]|false|Multiple values may be separated by commas.|
|game|query|integer|false|none|
|name|query|string|false|none|
|name__contains|query|string|false|none|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|
|user|query|integer|false|none|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "tiding": 0,
      "game": {
        "id": 0,
        "name": "string",
        "description": "string",
        "metaData": "string",
        "created": "2019-08-24T14:15:22Z",
        "releaseDate": "2019-08-24",
        "user": 0
      },
      "name": "string",
      "created": "2019-08-24T14:15:22Z",
      "user": 0
    }
  ]
}
```

<h3 id="highscores_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedHighScoreList](#schemapaginatedhighscorelist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## highscores_retrieve

<a id="opIdhighscores_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/highscores/{id}/', headers = headers)

print(r.json())

```

`GET /api/highscores/{id}/`

<h3 id="highscores_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "game": {
    "id": 0,
    "name": "string",
    "description": "string",
    "metaData": "string",
    "created": "2019-08-24T14:15:22Z",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "subscore_set": [
    {
      "pk": 0,
      "priority": -2147483648,
      "name": "string",
      "values": {
        "property1": null,
        "property2": null
      }
    }
  ],
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "user": 0
}
```

<h3 id="highscores_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[HighScoreFull](#schemahighscorefull)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## highscores_create_create

<a id="opIdhighscores_create_create"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'GAME-API': 'string',
  'Authorization': 'Bearer {access-token}'
}

r = requests.post('/api/highscores/create/', headers = headers)

print(r.json())

```

`POST /api/highscores/create/`

> Body parameter

```json
{
  "subscore_set": [
    {
      "priority": -2147483648,
      "name": "string",
      "values": {
        "property1": null,
        "property2": null
      }
    }
  ],
  "name": "string",
  "game": 0
}
```

```yaml
subscore_set:
  - priority: -2147483648
    name: string
    values:
      ? property1
      ? property2
name: string
game: 0

```

<h3 id="highscores_create_create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|GAME-API|header|string|false|Security key for POST highscores only from managed games|
|body|body|[HighScoreCreate](#schemahighscorecreate)|true|none|

> Example responses

> 201 Response

```json
{
  "id": 0,
  "subscore_set": [
    {
      "pk": 0,
      "priority": -2147483648,
      "name": "string",
      "values": {
        "property1": null,
        "property2": null
      }
    }
  ],
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "game": 0
}
```

<h3 id="highscores_create_create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|none|[HighScoreCreate](#schemahighscorecreate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## highscores_delete_destroy

<a id="opIdhighscores_delete_destroy"></a>

> Code samples

```python
import requests
headers = {
  'Authorization': 'Bearer {access-token}'
}

r = requests.delete('/api/highscores/delete/{id}/', headers = headers)

print(r.json())

```

`DELETE /api/highscores/delete/{id}/`

<h3 id="highscores_delete_destroy-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

<h3 id="highscores_delete_destroy-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|No response body|None|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## highscores_update_update

<a id="opIdhighscores_update_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/highscores/update/{id}/', headers = headers)

print(r.json())

```

`PUT /api/highscores/update/{id}/`

> Body parameter

```json
{
  "game": {
    "name": "string",
    "description": "string",
    "metaData": "string",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "name": "string",
  "user": 0
}
```

```yaml
game:
  name: string
  description: string
  metaData: string
  releaseDate: 2019-08-24
  user: 0
name: string
user: 0

```

<h3 id="highscores_update_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[HighScore](#schemahighscore)|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "game": {
    "id": 0,
    "name": "string",
    "description": "string",
    "metaData": "string",
    "created": "2019-08-24T14:15:22Z",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "user": 0
}
```

<h3 id="highscores_update_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[HighScore](#schemahighscore)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## highscores_update_partial_update

<a id="opIdhighscores_update_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/highscores/update/{id}/', headers = headers)

print(r.json())

```

`PATCH /api/highscores/update/{id}/`

> Body parameter

```json
{
  "game": {
    "name": "string",
    "description": "string",
    "metaData": "string",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "name": "string",
  "user": 0
}
```

```yaml
game:
  name: string
  description: string
  metaData: string
  releaseDate: 2019-08-24
  user: 0
name: string
user: 0

```

<h3 id="highscores_update_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[PatchedHighScore](#schemapatchedhighscore)|false|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "game": {
    "id": 0,
    "name": "string",
    "description": "string",
    "metaData": "string",
    "created": "2019-08-24T14:15:22Z",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "user": 0
}
```

<h3 id="highscores_update_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[HighScore](#schemahighscore)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## highscores_user_list

<a id="opIdhighscores_user_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/highscores/user/{userId}', headers = headers)

print(r.json())

```

`GET /api/highscores/user/{userId}`

<h3 id="highscores_user_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|
|userId|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "tiding": 0,
      "game": {
        "id": 0,
        "name": "string",
        "description": "string",
        "metaData": "string",
        "created": "2019-08-24T14:15:22Z",
        "releaseDate": "2019-08-24",
        "user": 0
      },
      "name": "string",
      "created": "2019-08-24T14:15:22Z",
      "user": 0
    }
  ]
}
```

<h3 id="highscores_user_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedHighScoreList](#schemapaginatedhighscorelist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

<h1 id="challenger-api-profile">profile</h1>

## profile_list

<a id="opIdprofile_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/profile/', headers = headers)

print(r.json())

```

`GET /api/profile/`

<h3 id="profile_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "user": {
        "id": 0,
        "username": "string",
        "last_login": "2019-08-24T14:15:22Z",
        "is_superuser": true,
        "first_name": "string",
        "last_name": "string",
        "email": "user@example.com",
        "is_staff": true,
        "is_active": true,
        "date_joined": "2019-08-24T14:15:22Z",
        "groups": [
          0
        ],
        "user_permissions": [
          0
        ]
      },
      "isBlocked": true,
      "isDeveloper": true,
      "avatar": "http://example.com",
      "bio": "string"
    }
  ]
}
```

<h3 id="profile_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedProfileSerializerAdminList](#schemapaginatedprofileserializeradminlist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## profile_retrieve

<a id="opIdprofile_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/profile/{user}/', headers = headers)

print(r.json())

```

`GET /api/profile/{user}/`

<h3 id="profile_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|user|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "user": {
    "id": 0,
    "username": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}
```

<h3 id="profile_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[ProfileSerializerAdmin](#schemaprofileserializeradmin)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## profile_current_retrieve

<a id="opIdprofile_current_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/profile/current/', headers = headers)

print(r.json())

```

`GET /api/profile/current/`

> Example responses

> 200 Response

```json
{
  "id": 0,
  "user": {
    "id": 0,
    "username": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}
```

<h3 id="profile_current_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[ProfileSerializerAdmin](#schemaprofileserializeradmin)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## profile_register_create

<a id="opIdprofile_register_create"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.post('/api/profile/register/', headers = headers)

print(r.json())

```

`POST /api/profile/register/`

> Body parameter

```json
{
  "username": "string",
  "password": "string",
  "email": "user@example.com"
}
```

```yaml
username: string
password: string
email: user@example.com

```

<h3 id="profile_register_create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[UserCreate](#schemausercreate)|true|none|

> Example responses

> 201 Response

```json
{
  "id": 0,
  "username": "string",
  "email": "user@example.com"
}
```

<h3 id="profile_register_create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|none|[UserCreate](#schemausercreate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## profile_update_update

<a id="opIdprofile_update_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/profile/update/{user}/', headers = headers)

print(r.json())

```

`PUT /api/profile/update/{user}/`

> Body parameter

```json
{
  "user": {
    "username": "string",
    "password": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}
```

```yaml
user:
  username: string
  password: string
  last_login: 2019-08-24T14:15:22Z
  is_superuser: true
  first_name: string
  last_name: string
  email: user@example.com
  is_staff: true
  is_active: true
  date_joined: 2019-08-24T14:15:22Z
  groups:
    - 0
  user_permissions:
    - 0
isBlocked: true
isDeveloper: true
avatar: http://example.com
bio: string

```

<h3 id="profile_update_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|user|path|integer|true|none|
|body|body|[ProfileSerializerAdmin](#schemaprofileserializeradmin)|false|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "user": {
    "id": 0,
    "username": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}
```

<h3 id="profile_update_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[ProfileSerializerAdmin](#schemaprofileserializeradmin)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## profile_update_partial_update

<a id="opIdprofile_update_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/profile/update/{user}/', headers = headers)

print(r.json())

```

`PATCH /api/profile/update/{user}/`

> Body parameter

```json
{
  "user": {
    "username": "string",
    "password": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}
```

```yaml
user:
  username: string
  password: string
  last_login: 2019-08-24T14:15:22Z
  is_superuser: true
  first_name: string
  last_name: string
  email: user@example.com
  is_staff: true
  is_active: true
  date_joined: 2019-08-24T14:15:22Z
  groups:
    - 0
  user_permissions:
    - 0
isBlocked: true
isDeveloper: true
avatar: http://example.com
bio: string

```

<h3 id="profile_update_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|user|path|integer|true|none|
|body|body|[PatchedProfileSerializerAdmin](#schemapatchedprofileserializeradmin)|false|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "user": {
    "id": 0,
    "username": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}
```

<h3 id="profile_update_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[ProfileSerializerAdmin](#schemaprofileserializeradmin)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## profile_update_current_update

<a id="opIdprofile_update_current_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/profile/update/current/', headers = headers)

print(r.json())

```

`PUT /api/profile/update/current/`

> Body parameter

```json
{
  "user": {
    "username": "string",
    "password": "string",
    "email": "user@example.com"
  },
  "avatar": "http://example.com",
  "bio": "string"
}
```

```yaml
user:
  username: string
  password: string
  email: user@example.com
avatar: http://example.com
bio: string

```

<h3 id="profile_update_current_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[Profile](#schemaprofile)|false|none|

> Example responses

> 200 Response

```json
{
  "user": {
    "id": 0,
    "username": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "is_superuser": true
  },
  "avatar": "http://example.com",
  "bio": "string"
}
```

<h3 id="profile_update_current_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[Profile](#schemaprofile)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## profile_update_current_partial_update

<a id="opIdprofile_update_current_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/profile/update/current/', headers = headers)

print(r.json())

```

`PATCH /api/profile/update/current/`

> Body parameter

```json
{
  "user": {
    "username": "string",
    "password": "string",
    "email": "user@example.com"
  },
  "avatar": "http://example.com",
  "bio": "string"
}
```

```yaml
user:
  username: string
  password: string
  email: user@example.com
avatar: http://example.com
bio: string

```

<h3 id="profile_update_current_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[PatchedProfile](#schemapatchedprofile)|false|none|

> Example responses

> 200 Response

```json
{
  "user": {
    "id": 0,
    "username": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "is_superuser": true
  },
  "avatar": "http://example.com",
  "bio": "string"
}
```

<h3 id="profile_update_current_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[Profile](#schemaprofile)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

<h1 id="challenger-api-tidings">tidings</h1>

## tidings_list

<a id="opIdtidings_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/tidings/', headers = headers)

print(r.json())

```

`GET /api/tidings/`

<h3 id="tidings_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|created__range|query|array[string]|false|Multiple values may be separated by commas.|
|highscore__isnull|query|boolean|false|none|
|message__isnull|query|boolean|false|none|
|news__isnull|query|boolean|false|none|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "allowComment": true,
      "highscore": 0,
      "news": 0,
      "message": 0
    }
  ]
}
```

<h3 id="tidings_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedTidingList](#schemapaginatedtidinglist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## tidings_retrieve

<a id="opIdtidings_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/tidings/{id}/', headers = headers)

print(r.json())

```

`GET /api/tidings/{id}/`

<h3 id="tidings_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "highscore": {
    "id": 0,
    "tiding": 0,
    "game": {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "user": 0
    },
    "subscore_set": [
      {
        "pk": 0,
        "priority": -2147483648,
        "name": "string",
        "values": {
          "property1": null,
          "property2": null
        }
      }
    ],
    "name": "string",
    "created": "2019-08-24T14:15:22Z",
    "user": 0
  },
  "news": {
    "id": 0,
    "game": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isActive": true,
    "dealID": "string",
    "dealRating": 0
  },
  "message": {
    "id": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isArchived": true,
    "content": "string"
  },
  "comment_set": [
    {
      "id": 0,
      "user": 0,
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "content": "string",
      "tiding": 0
    }
  ],
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true
}
```

<h3 id="tidings_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[TidingFull](#schematidingfull)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## tidings_adminmessage_create

<a id="opIdtidings_adminmessage_create"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.post('/api/tidings/adminmessage/', headers = headers)

print(r.json())

```

`POST /api/tidings/adminmessage/`

> Body parameter

```json
{
  "allowComment": true,
  "isArchived": true,
  "content": "string"
}
```

```yaml
allowComment: true
isArchived: true
content: string

```

<h3 id="tidings_adminmessage_create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[AdminMessage](#schemaadminmessage)|true|none|

> Example responses

> 201 Response

```json
{
  "id": 0,
  "tiding": 0,
  "created": "2019-08-24T14:15:22Z",
  "isArchived": true,
  "content": "string"
}
```

<h3 id="tidings_adminmessage_create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|none|[AdminMessage](#schemaadminmessage)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_adminmessage_retrieve

<a id="opIdtidings_adminmessage_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/tidings/adminmessage/{id}', headers = headers)

print(r.json())

```

`GET /api/tidings/adminmessage/{id}`

<h3 id="tidings_adminmessage_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "created": "2019-08-24T14:15:22Z",
  "isArchived": true,
  "content": "string"
}
```

<h3 id="tidings_adminmessage_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[AdminMessage](#schemaadminmessage)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_adminmessage_update

<a id="opIdtidings_adminmessage_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/tidings/adminmessage/{id}', headers = headers)

print(r.json())

```

`PUT /api/tidings/adminmessage/{id}`

> Body parameter

```json
{
  "allowComment": true,
  "isArchived": true,
  "content": "string"
}
```

```yaml
allowComment: true
isArchived: true
content: string

```

<h3 id="tidings_adminmessage_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[AdminMessage](#schemaadminmessage)|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "created": "2019-08-24T14:15:22Z",
  "isArchived": true,
  "content": "string"
}
```

<h3 id="tidings_adminmessage_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[AdminMessage](#schemaadminmessage)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_adminmessage_partial_update

<a id="opIdtidings_adminmessage_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/tidings/adminmessage/{id}', headers = headers)

print(r.json())

```

`PATCH /api/tidings/adminmessage/{id}`

> Body parameter

```json
{
  "allowComment": true,
  "isArchived": true,
  "content": "string"
}
```

```yaml
allowComment: true
isArchived: true
content: string

```

<h3 id="tidings_adminmessage_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[PatchedAdminMessage](#schemapatchedadminmessage)|false|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "created": "2019-08-24T14:15:22Z",
  "isArchived": true,
  "content": "string"
}
```

<h3 id="tidings_adminmessage_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[AdminMessage](#schemaadminmessage)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_adminmessage_destroy

<a id="opIdtidings_adminmessage_destroy"></a>

> Code samples

```python
import requests
headers = {
  'Authorization': 'Bearer {access-token}'
}

r = requests.delete('/api/tidings/adminmessage/{id}', headers = headers)

print(r.json())

```

`DELETE /api/tidings/adminmessage/{id}`

<h3 id="tidings_adminmessage_destroy-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

<h3 id="tidings_adminmessage_destroy-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|No response body|None|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_comment_retrieve

<a id="opIdtidings_comment_retrieve"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/tidings/comment/{id}/', headers = headers)

print(r.json())

```

`GET /api/tidings/comment/{id}/`

<h3 id="tidings_comment_retrieve-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "user": 0,
  "content": "string"
}
```

<h3 id="tidings_comment_retrieve-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[CommentCreate](#schemacommentcreate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_comment_update

<a id="opIdtidings_comment_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/tidings/comment/{id}/', headers = headers)

print(r.json())

```

`PUT /api/tidings/comment/{id}/`

> Body parameter

```json
{
  "tiding": 0,
  "content": "string"
}
```

```yaml
tiding: 0
content: string

```

<h3 id="tidings_comment_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[CommentCreate](#schemacommentcreate)|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "user": 0,
  "content": "string"
}
```

<h3 id="tidings_comment_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[CommentCreate](#schemacommentcreate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_comment_partial_update

<a id="opIdtidings_comment_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/tidings/comment/{id}/', headers = headers)

print(r.json())

```

`PATCH /api/tidings/comment/{id}/`

> Body parameter

```json
{
  "tiding": 0,
  "content": "string"
}
```

```yaml
tiding: 0
content: string

```

<h3 id="tidings_comment_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[PatchedCommentCreate](#schemapatchedcommentcreate)|false|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "tiding": 0,
  "user": 0,
  "content": "string"
}
```

<h3 id="tidings_comment_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[CommentCreate](#schemacommentcreate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_comment_destroy

<a id="opIdtidings_comment_destroy"></a>

> Code samples

```python
import requests
headers = {
  'Authorization': 'Bearer {access-token}'
}

r = requests.delete('/api/tidings/comment/{id}/', headers = headers)

print(r.json())

```

`DELETE /api/tidings/comment/{id}/`

<h3 id="tidings_comment_destroy-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|

<h3 id="tidings_comment_destroy-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|204|[No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5)|No response body|None|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_comment_create_create

<a id="opIdtidings_comment_create_create"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.post('/api/tidings/comment/create/', headers = headers)

print(r.json())

```

`POST /api/tidings/comment/create/`

> Body parameter

```json
{
  "tiding": 0,
  "content": "string"
}
```

```yaml
tiding: 0
content: string

```

<h3 id="tidings_comment_create_create-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[CommentCreate](#schemacommentcreate)|true|none|

> Example responses

> 201 Response

```json
{
  "id": 0,
  "tiding": 0,
  "user": 0,
  "content": "string"
}
```

<h3 id="tidings_comment_create_create-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|none|[CommentCreate](#schemacommentcreate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_full_list

<a id="opIdtidings_full_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/tidings/full', headers = headers)

print(r.json())

```

`GET /api/tidings/full`

<h3 id="tidings_full_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|created__range|query|array[string]|false|Multiple values may be separated by commas.|
|highscore__isnull|query|boolean|false|none|
|message__isnull|query|boolean|false|none|
|news__isnull|query|boolean|false|none|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "highscore": {
        "id": 0,
        "tiding": 0,
        "game": {
          "id": 0,
          "name": "string",
          "description": "string",
          "metaData": "string",
          "created": "2019-08-24T14:15:22Z",
          "releaseDate": "2019-08-24",
          "user": 0
        },
        "subscore_set": [
          {
            "pk": 0,
            "priority": -2147483648,
            "name": "string",
            "values": {
              "property1": null,
              "property2": null
            }
          }
        ],
        "name": "string",
        "created": "2019-08-24T14:15:22Z",
        "user": 0
      },
      "news": {
        "id": 0,
        "game": 0,
        "tiding": 0,
        "created": "2019-08-24T14:15:22Z",
        "isActive": true,
        "dealID": "string",
        "dealRating": 0
      },
      "message": {
        "id": 0,
        "tiding": 0,
        "created": "2019-08-24T14:15:22Z",
        "isArchived": true,
        "content": "string"
      },
      "comment_set": [
        {
          "id": 0,
          "user": 0,
          "created": "2019-08-24T14:15:22Z",
          "isBlocked": true,
          "content": "string",
          "tiding": 0
        }
      ],
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "allowComment": true
    }
  ]
}
```

<h3 id="tidings_full_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedTidingFullList](#schemapaginatedtidingfulllist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## tidings_messages_list

<a id="opIdtidings_messages_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/tidings/messages', headers = headers)

print(r.json())

```

`GET /api/tidings/messages`

<h3 id="tidings_messages_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|content|query|string|false|none|
|content__contains|query|string|false|none|
|created__range|query|array[string]|false|Multiple values may be separated by commas.|
|isArchived|query|boolean|false|none|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "tiding": 0,
      "created": "2019-08-24T14:15:22Z",
      "isArchived": true,
      "content": "string"
    }
  ]
}
```

<h3 id="tidings_messages_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedAdminMessageList](#schemapaginatedadminmessagelist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## tidings_news_list

<a id="opIdtidings_news_list"></a>

> Code samples

```python
import requests
headers = {
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.get('/api/tidings/news', headers = headers)

print(r.json())

```

`GET /api/tidings/news`

<h3 id="tidings_news_list-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|c|query|string|false|The pagination cursor value.|
|created__range|query|array[string]|false|Multiple values may be separated by commas.|
|dealRating|query|number(float)|false|none|
|dealRating__gt|query|number(float)|false|none|
|dealRating__lt|query|number(float)|false|none|
|isActive|query|boolean|false|none|
|ordering|query|string|false|Which field to use when ordering the results.|
|page_size|query|integer|false|Number of results to return per page.|

> Example responses

> 200 Response

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "game": 0,
      "tiding": 0,
      "created": "2019-08-24T14:15:22Z",
      "isActive": true,
      "dealID": "string",
      "dealRating": 0
    }
  ]
}
```

<h3 id="tidings_news_list-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[PaginatedNewsList](#schemapaginatednewslist)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth, None
</aside>

## tidings_update_update

<a id="opIdtidings_update_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/tidings/update/{id}/', headers = headers)

print(r.json())

```

`PUT /api/tidings/update/{id}/`

> Body parameter

```json
{
  "highscore": {
    "game": {
      "name": "string",
      "description": "string",
      "metaData": "string",
      "releaseDate": "2019-08-24",
      "user": 0
    },
    "subscore_set": [
      {
        "priority": -2147483648,
        "name": "string",
        "values": {
          "property1": null,
          "property2": null
        }
      }
    ],
    "name": "string",
    "user": 0
  },
  "news": {
    "game": 0,
    "isActive": true,
    "dealID": "string",
    "dealRating": 0
  },
  "message": {
    "allowComment": true,
    "isArchived": true,
    "content": "string"
  },
  "comment_set": [
    {
      "isBlocked": true,
      "content": "string",
      "tiding": 0
    }
  ],
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true
}
```

```yaml
highscore:
  game:
    name: string
    description: string
    metaData: string
    releaseDate: 2019-08-24
    user: 0
  subscore_set:
    - priority: -2147483648
      name: string
      values:
        ? property1
        ? property2
  name: string
  user: 0
news:
  game: 0
  isActive: true
  dealID: string
  dealRating: 0
message:
  allowComment: true
  isArchived: true
  content: string
comment_set:
  - isBlocked: true
    content: string
    tiding: 0
created: 2019-08-24T14:15:22Z
isBlocked: true
allowComment: true

```

<h3 id="tidings_update_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[TidingFull](#schematidingfull)|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "highscore": {
    "id": 0,
    "tiding": 0,
    "game": {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "user": 0
    },
    "subscore_set": [
      {
        "pk": 0,
        "priority": -2147483648,
        "name": "string",
        "values": {
          "property1": null,
          "property2": null
        }
      }
    ],
    "name": "string",
    "created": "2019-08-24T14:15:22Z",
    "user": 0
  },
  "news": {
    "id": 0,
    "game": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isActive": true,
    "dealID": "string",
    "dealRating": 0
  },
  "message": {
    "id": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isArchived": true,
    "content": "string"
  },
  "comment_set": [
    {
      "id": 0,
      "user": 0,
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "content": "string",
      "tiding": 0
    }
  ],
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true
}
```

<h3 id="tidings_update_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[TidingFull](#schematidingfull)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_update_partial_update

<a id="opIdtidings_update_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/tidings/update/{id}/', headers = headers)

print(r.json())

```

`PATCH /api/tidings/update/{id}/`

> Body parameter

```json
{
  "highscore": {
    "game": {
      "name": "string",
      "description": "string",
      "metaData": "string",
      "releaseDate": "2019-08-24",
      "user": 0
    },
    "subscore_set": [
      {
        "priority": -2147483648,
        "name": "string",
        "values": {
          "property1": null,
          "property2": null
        }
      }
    ],
    "name": "string",
    "user": 0
  },
  "news": {
    "game": 0,
    "isActive": true,
    "dealID": "string",
    "dealRating": 0
  },
  "message": {
    "allowComment": true,
    "isArchived": true,
    "content": "string"
  },
  "comment_set": [
    {
      "isBlocked": true,
      "content": "string",
      "tiding": 0
    }
  ],
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true
}
```

```yaml
highscore:
  game:
    name: string
    description: string
    metaData: string
    releaseDate: 2019-08-24
    user: 0
  subscore_set:
    - priority: -2147483648
      name: string
      values:
        ? property1
        ? property2
  name: string
  user: 0
news:
  game: 0
  isActive: true
  dealID: string
  dealRating: 0
message:
  allowComment: true
  isArchived: true
  content: string
comment_set:
  - isBlocked: true
    content: string
    tiding: 0
created: 2019-08-24T14:15:22Z
isBlocked: true
allowComment: true

```

<h3 id="tidings_update_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[PatchedTidingFull](#schemapatchedtidingfull)|false|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "highscore": {
    "id": 0,
    "tiding": 0,
    "game": {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "user": 0
    },
    "subscore_set": [
      {
        "pk": 0,
        "priority": -2147483648,
        "name": "string",
        "values": {
          "property1": null,
          "property2": null
        }
      }
    ],
    "name": "string",
    "created": "2019-08-24T14:15:22Z",
    "user": 0
  },
  "news": {
    "id": 0,
    "game": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isActive": true,
    "dealID": "string",
    "dealRating": 0
  },
  "message": {
    "id": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isArchived": true,
    "content": "string"
  },
  "comment_set": [
    {
      "id": 0,
      "user": 0,
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "content": "string",
      "tiding": 0
    }
  ],
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true
}
```

<h3 id="tidings_update_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[TidingFull](#schematidingfull)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_update_own_update

<a id="opIdtidings_update_own_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.put('/api/tidings/update/own/{id}/', headers = headers)

print(r.json())

```

`PUT /api/tidings/update/own/{id}/`

> Body parameter

```json
{
  "allowComment": true
}
```

```yaml
allowComment: true

```

<h3 id="tidings_update_own_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[TidingSerializerUserUpdate](#schematidingserializeruserupdate)|false|none|

> Example responses

> 200 Response

```json
{
  "allowComment": true
}
```

<h3 id="tidings_update_own_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[TidingSerializerUserUpdate](#schematidingserializeruserupdate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

## tidings_update_own_partial_update

<a id="opIdtidings_update_own_partial_update"></a>

> Code samples

```python
import requests
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': 'Bearer {access-token}'
}

r = requests.patch('/api/tidings/update/own/{id}/', headers = headers)

print(r.json())

```

`PATCH /api/tidings/update/own/{id}/`

> Body parameter

```json
{
  "allowComment": true
}
```

```yaml
allowComment: true

```

<h3 id="tidings_update_own_partial_update-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer|true|none|
|body|body|[PatchedTidingSerializerUserUpdate](#schemapatchedtidingserializeruserupdate)|false|none|

> Example responses

> 200 Response

```json
{
  "allowComment": true
}
```

<h3 id="tidings_update_own_partial_update-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|none|[TidingSerializerUserUpdate](#schematidingserializeruserupdate)|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
jwtAuth
</aside>

# Schemas

<h2 id="tocS_AdminMessage">AdminMessage</h2>
<!-- backwards compatibility -->
<a id="schemaadminmessage"></a>
<a id="schema_AdminMessage"></a>
<a id="tocSadminmessage"></a>
<a id="tocsadminmessage"></a>

```json
{
  "id": 0,
  "allowComment": true,
  "tiding": 0,
  "created": "2019-08-24T14:15:22Z",
  "isArchived": true,
  "content": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|allowComment|boolean|false|write-only|none|
|tiding|integer|true|read-only|none|
|created|string(date-time)|true|read-only|none|
|isArchived|boolean|false|none|none|
|content|string|true|none|none|

<h2 id="tocS_CommentCreate">CommentCreate</h2>
<!-- backwards compatibility -->
<a id="schemacommentcreate"></a>
<a id="schema_CommentCreate"></a>
<a id="tocScommentcreate"></a>
<a id="tocscommentcreate"></a>

```json
{
  "id": 0,
  "tiding": 0,
  "user": 0,
  "content": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|tiding|integer|true|none|none|
|user|integer|true|read-only|none|
|content|string|true|none|none|

<h2 id="tocS_CommentManage">CommentManage</h2>
<!-- backwards compatibility -->
<a id="schemacommentmanage"></a>
<a id="schema_CommentManage"></a>
<a id="tocScommentmanage"></a>
<a id="tocscommentmanage"></a>

```json
{
  "id": 0,
  "user": 0,
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "content": "string",
  "tiding": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|user|integer|true|read-only|none|
|created|string(date-time)|true|read-only|none|
|isBlocked|boolean|false|none|none|
|content|string|true|none|none|
|tiding|integer|true|none|none|

<h2 id="tocS_Game">Game</h2>
<!-- backwards compatibility -->
<a id="schemagame"></a>
<a id="schema_Game"></a>
<a id="tocSgame"></a>
<a id="tocsgame"></a>

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "user": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|name|string|true|none|none|
|description|string|false|none|none|
|metaData|string|false|none|none|
|created|string(date-time)|true|read-only|none|
|releaseDate|string(date)¦null|false|none|none|
|user|integer¦null|false|none|none|

<h2 id="tocS_GameCreate">GameCreate</h2>
<!-- backwards compatibility -->
<a id="schemagamecreate"></a>
<a id="schema_GameCreate"></a>
<a id="tocSgamecreate"></a>
<a id="tocsgamecreate"></a>

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|name|string|true|none|none|
|description|string|false|none|none|
|metaData|string|false|none|none|
|created|string(date-time)|true|read-only|none|
|releaseDate|string(date)¦null|false|none|none|
|apiKey|string|true|read-only|none|
|user|integer¦null|true|read-only|none|

<h2 id="tocS_GameDeveloper">GameDeveloper</h2>
<!-- backwards compatibility -->
<a id="schemagamedeveloper"></a>
<a id="schema_GameDeveloper"></a>
<a id="tocSgamedeveloper"></a>
<a id="tocsgamedeveloper"></a>

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|name|string|true|none|none|
|description|string|false|none|none|
|metaData|string|false|none|none|
|created|string(date-time)|true|read-only|none|
|releaseDate|string(date)¦null|false|none|none|
|apiKey|string|false|none|none|
|user|integer¦null|false|none|none|

<h2 id="tocS_HighScore">HighScore</h2>
<!-- backwards compatibility -->
<a id="schemahighscore"></a>
<a id="schema_HighScore"></a>
<a id="tocShighscore"></a>
<a id="tocshighscore"></a>

```json
{
  "id": 0,
  "tiding": 0,
  "game": {
    "id": 0,
    "name": "string",
    "description": "string",
    "metaData": "string",
    "created": "2019-08-24T14:15:22Z",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "user": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|tiding|integer|true|read-only|none|
|game|[Game](#schemagame)|true|none|none|
|name|string|true|none|none|
|created|string(date-time)|true|read-only|none|
|user|integer¦null|false|none|none|

<h2 id="tocS_HighScoreCreate">HighScoreCreate</h2>
<!-- backwards compatibility -->
<a id="schemahighscorecreate"></a>
<a id="schema_HighScoreCreate"></a>
<a id="tocShighscorecreate"></a>
<a id="tocshighscorecreate"></a>

```json
{
  "id": 0,
  "subscore_set": [
    {
      "pk": 0,
      "priority": -2147483648,
      "name": "string",
      "values": {
        "property1": null,
        "property2": null
      }
    }
  ],
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "game": 0
}

```

Adds nested create feature

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|subscore_set|[[SubScore](#schemasubscore)]|true|none|none|
|name|string|true|none|none|
|created|string(date-time)|true|read-only|none|
|game|integer¦null|false|none|none|

<h2 id="tocS_HighScoreFull">HighScoreFull</h2>
<!-- backwards compatibility -->
<a id="schemahighscorefull"></a>
<a id="schema_HighScoreFull"></a>
<a id="tocShighscorefull"></a>
<a id="tocshighscorefull"></a>

```json
{
  "id": 0,
  "tiding": 0,
  "game": {
    "id": 0,
    "name": "string",
    "description": "string",
    "metaData": "string",
    "created": "2019-08-24T14:15:22Z",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "subscore_set": [
    {
      "pk": 0,
      "priority": -2147483648,
      "name": "string",
      "values": {
        "property1": null,
        "property2": null
      }
    }
  ],
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "user": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|tiding|integer|true|read-only|none|
|game|[Game](#schemagame)|true|none|none|
|subscore_set|[[SubScore](#schemasubscore)]|true|none|none|
|name|string|true|none|none|
|created|string(date-time)|true|read-only|none|
|user|integer¦null|false|none|none|

<h2 id="tocS_News">News</h2>
<!-- backwards compatibility -->
<a id="schemanews"></a>
<a id="schema_News"></a>
<a id="tocSnews"></a>
<a id="tocsnews"></a>

```json
{
  "id": 0,
  "game": 0,
  "tiding": 0,
  "created": "2019-08-24T14:15:22Z",
  "isActive": true,
  "dealID": "string",
  "dealRating": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|game|integer|true|none|none|
|tiding|integer|true|read-only|none|
|created|string(date-time)|true|read-only|none|
|isActive|boolean|false|none|none|
|dealID|string¦null|false|none|none|
|dealRating|number(double)|false|none|none|

<h2 id="tocS_PaginatedAdminMessageList">PaginatedAdminMessageList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatedadminmessagelist"></a>
<a id="schema_PaginatedAdminMessageList"></a>
<a id="tocSpaginatedadminmessagelist"></a>
<a id="tocspaginatedadminmessagelist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "allowComment": true,
      "tiding": 0,
      "created": "2019-08-24T14:15:22Z",
      "isArchived": true,
      "content": "string"
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[AdminMessage](#schemaadminmessage)]|false|none|none|

<h2 id="tocS_PaginatedGameDeveloperList">PaginatedGameDeveloperList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatedgamedeveloperlist"></a>
<a id="schema_PaginatedGameDeveloperList"></a>
<a id="tocSpaginatedgamedeveloperlist"></a>
<a id="tocspaginatedgamedeveloperlist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "apiKey": "string",
      "user": 0
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[GameDeveloper](#schemagamedeveloper)]|false|none|none|

<h2 id="tocS_PaginatedGameList">PaginatedGameList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatedgamelist"></a>
<a id="schema_PaginatedGameList"></a>
<a id="tocSpaginatedgamelist"></a>
<a id="tocspaginatedgamelist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "user": 0
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[Game](#schemagame)]|false|none|none|

<h2 id="tocS_PaginatedHighScoreList">PaginatedHighScoreList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatedhighscorelist"></a>
<a id="schema_PaginatedHighScoreList"></a>
<a id="tocSpaginatedhighscorelist"></a>
<a id="tocspaginatedhighscorelist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "tiding": 0,
      "game": {
        "id": 0,
        "name": "string",
        "description": "string",
        "metaData": "string",
        "created": "2019-08-24T14:15:22Z",
        "releaseDate": "2019-08-24",
        "user": 0
      },
      "name": "string",
      "created": "2019-08-24T14:15:22Z",
      "user": 0
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[HighScore](#schemahighscore)]|false|none|none|

<h2 id="tocS_PaginatedNewsList">PaginatedNewsList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatednewslist"></a>
<a id="schema_PaginatedNewsList"></a>
<a id="tocSpaginatednewslist"></a>
<a id="tocspaginatednewslist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "game": 0,
      "tiding": 0,
      "created": "2019-08-24T14:15:22Z",
      "isActive": true,
      "dealID": "string",
      "dealRating": 0
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[News](#schemanews)]|false|none|none|

<h2 id="tocS_PaginatedProfileSerializerAdminList">PaginatedProfileSerializerAdminList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatedprofileserializeradminlist"></a>
<a id="schema_PaginatedProfileSerializerAdminList"></a>
<a id="tocSpaginatedprofileserializeradminlist"></a>
<a id="tocspaginatedprofileserializeradminlist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "user": {
        "id": 0,
        "username": "string",
        "password": "string",
        "last_login": "2019-08-24T14:15:22Z",
        "is_superuser": true,
        "first_name": "string",
        "last_name": "string",
        "email": "user@example.com",
        "is_staff": true,
        "is_active": true,
        "date_joined": "2019-08-24T14:15:22Z",
        "groups": [
          0
        ],
        "user_permissions": [
          0
        ]
      },
      "isBlocked": true,
      "isDeveloper": true,
      "avatar": "http://example.com",
      "bio": "string"
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[ProfileSerializerAdmin](#schemaprofileserializeradmin)]|false|none|[Adds nested create feature]|

<h2 id="tocS_PaginatedTidingFullList">PaginatedTidingFullList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatedtidingfulllist"></a>
<a id="schema_PaginatedTidingFullList"></a>
<a id="tocSpaginatedtidingfulllist"></a>
<a id="tocspaginatedtidingfulllist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "highscore": {
        "id": 0,
        "tiding": 0,
        "game": {
          "id": 0,
          "name": "string",
          "description": "string",
          "metaData": "string",
          "created": "2019-08-24T14:15:22Z",
          "releaseDate": "2019-08-24",
          "user": 0
        },
        "subscore_set": [
          {
            "pk": 0,
            "priority": -2147483648,
            "name": "string",
            "values": {
              "property1": null,
              "property2": null
            }
          }
        ],
        "name": "string",
        "created": "2019-08-24T14:15:22Z",
        "user": 0
      },
      "news": {
        "id": 0,
        "game": 0,
        "tiding": 0,
        "created": "2019-08-24T14:15:22Z",
        "isActive": true,
        "dealID": "string",
        "dealRating": 0
      },
      "message": {
        "id": 0,
        "allowComment": true,
        "tiding": 0,
        "created": "2019-08-24T14:15:22Z",
        "isArchived": true,
        "content": "string"
      },
      "comment_set": [
        {
          "id": 0,
          "user": 0,
          "created": "2019-08-24T14:15:22Z",
          "isBlocked": true,
          "content": "string",
          "tiding": 0
        }
      ],
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "allowComment": true
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[TidingFull](#schematidingfull)]|false|none|none|

<h2 id="tocS_PaginatedTidingList">PaginatedTidingList</h2>
<!-- backwards compatibility -->
<a id="schemapaginatedtidinglist"></a>
<a id="schema_PaginatedTidingList"></a>
<a id="tocSpaginatedtidinglist"></a>
<a id="tocspaginatedtidinglist"></a>

```json
{
  "next": "string",
  "previous": "string",
  "results": [
    {
      "id": 0,
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "allowComment": true,
      "highscore": 0,
      "news": 0,
      "message": 0
    }
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|next|string¦null|false|none|none|
|previous|string¦null|false|none|none|
|results|[[Tiding](#schematiding)]|false|none|none|

<h2 id="tocS_PatchedAdminMessage">PatchedAdminMessage</h2>
<!-- backwards compatibility -->
<a id="schemapatchedadminmessage"></a>
<a id="schema_PatchedAdminMessage"></a>
<a id="tocSpatchedadminmessage"></a>
<a id="tocspatchedadminmessage"></a>

```json
{
  "id": 0,
  "allowComment": true,
  "tiding": 0,
  "created": "2019-08-24T14:15:22Z",
  "isArchived": true,
  "content": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|false|read-only|none|
|allowComment|boolean|false|write-only|none|
|tiding|integer|false|read-only|none|
|created|string(date-time)|false|read-only|none|
|isArchived|boolean|false|none|none|
|content|string|false|none|none|

<h2 id="tocS_PatchedCommentCreate">PatchedCommentCreate</h2>
<!-- backwards compatibility -->
<a id="schemapatchedcommentcreate"></a>
<a id="schema_PatchedCommentCreate"></a>
<a id="tocSpatchedcommentcreate"></a>
<a id="tocspatchedcommentcreate"></a>

```json
{
  "id": 0,
  "tiding": 0,
  "user": 0,
  "content": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|false|read-only|none|
|tiding|integer|false|none|none|
|user|integer|false|read-only|none|
|content|string|false|none|none|

<h2 id="tocS_PatchedGameDeveloper">PatchedGameDeveloper</h2>
<!-- backwards compatibility -->
<a id="schemapatchedgamedeveloper"></a>
<a id="schema_PatchedGameDeveloper"></a>
<a id="tocSpatchedgamedeveloper"></a>
<a id="tocspatchedgamedeveloper"></a>

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "metaData": "string",
  "created": "2019-08-24T14:15:22Z",
  "releaseDate": "2019-08-24",
  "apiKey": "string",
  "user": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|false|read-only|none|
|name|string|false|none|none|
|description|string|false|none|none|
|metaData|string|false|none|none|
|created|string(date-time)|false|read-only|none|
|releaseDate|string(date)¦null|false|none|none|
|apiKey|string|false|none|none|
|user|integer¦null|false|none|none|

<h2 id="tocS_PatchedHighScore">PatchedHighScore</h2>
<!-- backwards compatibility -->
<a id="schemapatchedhighscore"></a>
<a id="schema_PatchedHighScore"></a>
<a id="tocSpatchedhighscore"></a>
<a id="tocspatchedhighscore"></a>

```json
{
  "id": 0,
  "tiding": 0,
  "game": {
    "id": 0,
    "name": "string",
    "description": "string",
    "metaData": "string",
    "created": "2019-08-24T14:15:22Z",
    "releaseDate": "2019-08-24",
    "user": 0
  },
  "name": "string",
  "created": "2019-08-24T14:15:22Z",
  "user": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|false|read-only|none|
|tiding|integer|false|read-only|none|
|game|[Game](#schemagame)|false|none|none|
|name|string|false|none|none|
|created|string(date-time)|false|read-only|none|
|user|integer¦null|false|none|none|

<h2 id="tocS_PatchedProfile">PatchedProfile</h2>
<!-- backwards compatibility -->
<a id="schemapatchedprofile"></a>
<a id="schema_PatchedProfile"></a>
<a id="tocSpatchedprofile"></a>
<a id="tocspatchedprofile"></a>

```json
{
  "user": {
    "id": 0,
    "username": "string",
    "password": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "is_superuser": true
  },
  "avatar": "http://example.com",
  "bio": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|user|[UserDetail](#schemauserdetail)|false|none|none|
|avatar|string(uri)|false|none|none|
|bio|string|false|none|none|

<h2 id="tocS_PatchedProfileSerializerAdmin">PatchedProfileSerializerAdmin</h2>
<!-- backwards compatibility -->
<a id="schemapatchedprofileserializeradmin"></a>
<a id="schema_PatchedProfileSerializerAdmin"></a>
<a id="tocSpatchedprofileserializeradmin"></a>
<a id="tocspatchedprofileserializeradmin"></a>

```json
{
  "id": 0,
  "user": {
    "id": 0,
    "username": "string",
    "password": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}

```

Adds nested create feature

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|false|read-only|none|
|user|[UserAdmin](#schemauseradmin)|false|none|none|
|isBlocked|boolean|false|none|none|
|isDeveloper|boolean|false|none|none|
|avatar|string(uri)|false|none|none|
|bio|string|false|none|none|

<h2 id="tocS_PatchedTidingFull">PatchedTidingFull</h2>
<!-- backwards compatibility -->
<a id="schemapatchedtidingfull"></a>
<a id="schema_PatchedTidingFull"></a>
<a id="tocSpatchedtidingfull"></a>
<a id="tocspatchedtidingfull"></a>

```json
{
  "id": 0,
  "highscore": {
    "id": 0,
    "tiding": 0,
    "game": {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "user": 0
    },
    "subscore_set": [
      {
        "pk": 0,
        "priority": -2147483648,
        "name": "string",
        "values": {
          "property1": null,
          "property2": null
        }
      }
    ],
    "name": "string",
    "created": "2019-08-24T14:15:22Z",
    "user": 0
  },
  "news": {
    "id": 0,
    "game": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isActive": true,
    "dealID": "string",
    "dealRating": 0
  },
  "message": {
    "id": 0,
    "allowComment": true,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isArchived": true,
    "content": "string"
  },
  "comment_set": [
    {
      "id": 0,
      "user": 0,
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "content": "string",
      "tiding": 0
    }
  ],
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|false|read-only|none|
|highscore|[HighScoreFull](#schemahighscorefull)|false|none|none|
|news|[News](#schemanews)|false|none|none|
|message|[AdminMessage](#schemaadminmessage)|false|none|none|
|comment_set|[[CommentManage](#schemacommentmanage)]|false|none|none|
|created|string(date-time)|false|none|none|
|isBlocked|boolean|false|none|none|
|allowComment|boolean|false|none|none|

<h2 id="tocS_PatchedTidingSerializerUserUpdate">PatchedTidingSerializerUserUpdate</h2>
<!-- backwards compatibility -->
<a id="schemapatchedtidingserializeruserupdate"></a>
<a id="schema_PatchedTidingSerializerUserUpdate"></a>
<a id="tocSpatchedtidingserializeruserupdate"></a>
<a id="tocspatchedtidingserializeruserupdate"></a>

```json
{
  "allowComment": true
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|allowComment|boolean|false|none|none|

<h2 id="tocS_Profile">Profile</h2>
<!-- backwards compatibility -->
<a id="schemaprofile"></a>
<a id="schema_Profile"></a>
<a id="tocSprofile"></a>
<a id="tocsprofile"></a>

```json
{
  "user": {
    "id": 0,
    "username": "string",
    "password": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "is_superuser": true
  },
  "avatar": "http://example.com",
  "bio": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|user|[UserDetail](#schemauserdetail)|false|none|none|
|avatar|string(uri)|false|none|none|
|bio|string|false|none|none|

<h2 id="tocS_ProfileSerializerAdmin">ProfileSerializerAdmin</h2>
<!-- backwards compatibility -->
<a id="schemaprofileserializeradmin"></a>
<a id="schema_ProfileSerializerAdmin"></a>
<a id="tocSprofileserializeradmin"></a>
<a id="tocsprofileserializeradmin"></a>

```json
{
  "id": 0,
  "user": {
    "id": 0,
    "username": "string",
    "password": "string",
    "last_login": "2019-08-24T14:15:22Z",
    "is_superuser": true,
    "first_name": "string",
    "last_name": "string",
    "email": "user@example.com",
    "is_staff": true,
    "is_active": true,
    "date_joined": "2019-08-24T14:15:22Z",
    "groups": [
      0
    ],
    "user_permissions": [
      0
    ]
  },
  "isBlocked": true,
  "isDeveloper": true,
  "avatar": "http://example.com",
  "bio": "string"
}

```

Adds nested create feature

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|user|[UserAdmin](#schemauseradmin)|false|none|none|
|isBlocked|boolean|false|none|none|
|isDeveloper|boolean|false|none|none|
|avatar|string(uri)|false|none|none|
|bio|string|false|none|none|

<h2 id="tocS_SubScore">SubScore</h2>
<!-- backwards compatibility -->
<a id="schemasubscore"></a>
<a id="schema_SubScore"></a>
<a id="tocSsubscore"></a>
<a id="tocssubscore"></a>

```json
{
  "pk": 0,
  "priority": -2147483648,
  "name": "string",
  "values": {
    "property1": null,
    "property2": null
  }
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|pk|integer|true|read-only|none|
|priority|integer|false|none|none|
|name|string|true|none|none|
|values|object¦null|false|none|none|
|» **additionalProperties**|any|false|none|none|

<h2 id="tocS_Tiding">Tiding</h2>
<!-- backwards compatibility -->
<a id="schematiding"></a>
<a id="schema_Tiding"></a>
<a id="tocStiding"></a>
<a id="tocstiding"></a>

```json
{
  "id": 0,
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true,
  "highscore": 0,
  "news": 0,
  "message": 0
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|created|string(date-time)|true|none|none|
|isBlocked|boolean|false|none|none|
|allowComment|boolean|false|none|none|
|highscore|integer¦null|false|none|none|
|news|integer¦null|false|none|none|
|message|integer¦null|false|none|none|

<h2 id="tocS_TidingFull">TidingFull</h2>
<!-- backwards compatibility -->
<a id="schematidingfull"></a>
<a id="schema_TidingFull"></a>
<a id="tocStidingfull"></a>
<a id="tocstidingfull"></a>

```json
{
  "id": 0,
  "highscore": {
    "id": 0,
    "tiding": 0,
    "game": {
      "id": 0,
      "name": "string",
      "description": "string",
      "metaData": "string",
      "created": "2019-08-24T14:15:22Z",
      "releaseDate": "2019-08-24",
      "user": 0
    },
    "subscore_set": [
      {
        "pk": 0,
        "priority": -2147483648,
        "name": "string",
        "values": {
          "property1": null,
          "property2": null
        }
      }
    ],
    "name": "string",
    "created": "2019-08-24T14:15:22Z",
    "user": 0
  },
  "news": {
    "id": 0,
    "game": 0,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isActive": true,
    "dealID": "string",
    "dealRating": 0
  },
  "message": {
    "id": 0,
    "allowComment": true,
    "tiding": 0,
    "created": "2019-08-24T14:15:22Z",
    "isArchived": true,
    "content": "string"
  },
  "comment_set": [
    {
      "id": 0,
      "user": 0,
      "created": "2019-08-24T14:15:22Z",
      "isBlocked": true,
      "content": "string",
      "tiding": 0
    }
  ],
  "created": "2019-08-24T14:15:22Z",
  "isBlocked": true,
  "allowComment": true
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|highscore|[HighScoreFull](#schemahighscorefull)|true|none|none|
|news|[News](#schemanews)|true|none|none|
|message|[AdminMessage](#schemaadminmessage)|true|none|none|
|comment_set|[[CommentManage](#schemacommentmanage)]|true|none|none|
|created|string(date-time)|true|none|none|
|isBlocked|boolean|false|none|none|
|allowComment|boolean|false|none|none|

<h2 id="tocS_TidingSerializerUserUpdate">TidingSerializerUserUpdate</h2>
<!-- backwards compatibility -->
<a id="schematidingserializeruserupdate"></a>
<a id="schema_TidingSerializerUserUpdate"></a>
<a id="tocStidingserializeruserupdate"></a>
<a id="tocstidingserializeruserupdate"></a>

```json
{
  "allowComment": true
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|allowComment|boolean|false|none|none|

<h2 id="tocS_TokenObtainPair">TokenObtainPair</h2>
<!-- backwards compatibility -->
<a id="schematokenobtainpair"></a>
<a id="schema_TokenObtainPair"></a>
<a id="tocStokenobtainpair"></a>
<a id="tocstokenobtainpair"></a>

```json
{
  "username": "string",
  "password": "string",
  "access": "string",
  "refresh": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|username|string|true|write-only|none|
|password|string|true|write-only|none|
|access|string|true|read-only|none|
|refresh|string|true|read-only|none|

<h2 id="tocS_TokenRefresh">TokenRefresh</h2>
<!-- backwards compatibility -->
<a id="schematokenrefresh"></a>
<a id="schema_TokenRefresh"></a>
<a id="tocStokenrefresh"></a>
<a id="tocstokenrefresh"></a>

```json
{
  "access": "string",
  "refresh": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|access|string|true|read-only|none|
|refresh|string|true|none|none|

<h2 id="tocS_UserAdmin">UserAdmin</h2>
<!-- backwards compatibility -->
<a id="schemauseradmin"></a>
<a id="schema_UserAdmin"></a>
<a id="tocSuseradmin"></a>
<a id="tocsuseradmin"></a>

```json
{
  "id": 0,
  "username": "string",
  "password": "string",
  "last_login": "2019-08-24T14:15:22Z",
  "is_superuser": true,
  "first_name": "string",
  "last_name": "string",
  "email": "user@example.com",
  "is_staff": true,
  "is_active": true,
  "date_joined": "2019-08-24T14:15:22Z",
  "groups": [
    0
  ],
  "user_permissions": [
    0
  ]
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|username|string|false|none|none|
|password|string|false|write-only|none|
|last_login|string(date-time)¦null|false|none|none|
|is_superuser|boolean|false|none|Designates that this user has all permissions without explicitly assigning them.|
|first_name|string|false|none|none|
|last_name|string|false|none|none|
|email|string(email)|false|none|none|
|is_staff|boolean|false|none|Designates whether the user can log into this admin site.|
|is_active|boolean|false|none|Designates whether this user should be treated as active. Unselect this instead of deleting accounts.|
|date_joined|string(date-time)|false|none|none|
|groups|[integer]|false|none|The groups this user belongs to. A user will get all permissions granted to each of their groups.|
|user_permissions|[integer]|false|none|Specific permissions for this user.|

<h2 id="tocS_UserCreate">UserCreate</h2>
<!-- backwards compatibility -->
<a id="schemausercreate"></a>
<a id="schema_UserCreate"></a>
<a id="tocSusercreate"></a>
<a id="tocsusercreate"></a>

```json
{
  "id": 0,
  "username": "string",
  "password": "string",
  "email": "user@example.com"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|username|string|true|none|Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.|
|password|string|true|write-only|none|
|email|string(email)|false|none|none|

<h2 id="tocS_UserDetail">UserDetail</h2>
<!-- backwards compatibility -->
<a id="schemauserdetail"></a>
<a id="schema_UserDetail"></a>
<a id="tocSuserdetail"></a>
<a id="tocsuserdetail"></a>

```json
{
  "id": 0,
  "username": "string",
  "password": "string",
  "email": "user@example.com",
  "is_staff": true,
  "is_active": true,
  "is_superuser": true
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer|true|read-only|none|
|username|string|false|none|none|
|password|string|false|write-only|none|
|email|string(email)|false|none|none|
|is_staff|boolean|true|read-only|Designates whether the user can log into this admin site.|
|is_active|boolean|true|read-only|Designates whether this user should be treated as active. Unselect this instead of deleting accounts.|
|is_superuser|boolean|true|read-only|Designates that this user has all permissions without explicitly assigning them.|

