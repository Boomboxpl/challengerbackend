import json
import os
import sys
from types import SimpleNamespace
from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore , register_events
from django.utils import timezone
from django_apscheduler.models import DjangoJobExecution
from tidings.refresh import refreshNews
from games.igdb import refreshToken
from django_apscheduler import util

isStarted = False

# This is the function you want to schedule - add as many as you want and then register them in the start() function below
@util.close_old_connections
def refreshTidings():
    print("Refreshing news", file=sys.stdout)
    refreshNews()
    
@util.close_old_connections
def refreshIGDBToken():
    print("Refreshing token", file=sys.stdout)
    refreshToken()
    
def start():
    global isStarted
    if not isStarted:
        isStarted = True
        scheduler = BackgroundScheduler()
        store = DjangoJobStore()
        scheduler.add_jobstore(store, "default")
        scheduler.remove_all_jobs()
        scheduler.add_job(refreshTidings, 
                          'interval', 
                          minutes = 31,
                          name='refresh_news', 
                          jobstore='default', 
                          id = "refresh_news", 
                          max_instances= 1,
                          replace_existing= True)
        scheduler.add_job(refreshIGDBToken, 
                          'interval', 
                          days = 1, 
                          name='refresh_token', 
                          jobstore='default', 
                          id = "refresh_token", 
                          max_instances= 1,
                          replace_existing= True)
        register_events(scheduler)
        scheduler.start()
        refreshIGDBToken()
#        refreshNews()
        print("Scheduler started...", file=sys.stdout)