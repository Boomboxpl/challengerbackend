from django.core.management.base import BaseCommand
from subprocess import Popen
from sys import stdout, stdin, stderr
import time
import os
import signal


class Command(BaseCommand):
    help = 'Run all commands'
    commands = [
        'python manage.py migrate --noinput',
        'python manage.py createcachetable'
    ]

    def handle(self, *args, **options):
        proc_list = []
        
        for command in self.commands:
            print("$ " + command)
            proc = Popen(command, shell=True, stdin=stdin,
                         stdout=stdout, stderr=stderr)
            proc.communicate()