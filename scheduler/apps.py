import sys
from django.apps import AppConfig

class SchedulerConfig(AppConfig):
    name = 'scheduler'
    blockedCommands=['makemigrations',
                     'migrate',
                     'createcachetable',
                     'complexmigrate',
                     'help',
                     'changepassword',
                     'createsuperuser',
                     'collectstatic']
    
    def ready(self):
        for com in self.blockedCommands:
            if com in sys.argv:
                return
        from scheduler import scheduler
        scheduler.start()