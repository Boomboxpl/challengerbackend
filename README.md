# Challenger
Project for PPP, OIRPOS, PAI Subject. Authors: Filip Gaida, Łukasz Kubicki, Paweł Woch

# Description
This project is REST API server application for tracking game high scores and share it with other application users. Application track game sales using CheapSharkAPI (https://apidocs.cheapshark.com/) and serves additional game data from IGDB API (https://www.igdb.com/api). Application content is managed by Admin either by front-end site or {host}/admin. Application use Cloudinary CDN for static files.

Design document: https://docs.google.com/document/d/1V0YMQGTfUYAtRUz6OK7Tko05TRr2K5DgrRQvB3jsRsA/edit?usp=sharing

## Important urls
- {host}/admin/
- {host}/api/schema/swagger-ui/

# Related projects
Front-end: https://gitlab.com/kubickilukasz/vueproject2022

Submodule example for Unity enviroment: https://gitlab.com/Boomboxpl/challengerapiunity

# API docs
In repository: [API](ChallengerAPI.md)

Using development Swagger: https://challenger-backend.fly.dev/api/schema/swagger-ui/#

# Install and run
1. Clone repository ```git clone https://gitlab.com/Boomboxpl/challengerapiunity.git```
2. Instal Python packages ```pip install -r requirements.txt```
3. Create super user and migrate data using Django manage.py
4. Fill data for enviroment:
- PostgreSQL login data (PG_HOST, PG_NAME, PG_USER, PG_PASSWORD, PG_PORT)
- Cloudinary API data (CLOUD_NAME, CLOUD_KEY, CLOUD_SECRET)
- Debug option (RUN_DEBUG)
- IGDB API data (IGDB_ID, IGDB_PASSWORD)
5. Run application using one of following
- ```python manage.py runserver --noreload```
- ```gunicorn -b 0.0.0.0:8080 --workers 3 challenger.wsgi``` or other Python WSGI HTTP server

# Run using fly.io
Application is default deployed with fly.io. This step require ```flyctl``` application. More information about creating fly.io applications can be found here: https://fly.io/.

Steps to deploy:

1. Launch standard application with postgress database ```fly launch```
2. Setup all secret data (change =_ to =value) ```flyctl secrets set PG_HOST = PG_NAME=_ PG_USER=_ PG_PASSWORD=_ PG_PORT=_ CLOUD_NAME=_ CLOUD_KEY=_ CLOUD_SECRET=_ IGDB_ID=_ IGDB_PASSWORD=_```
3. Deploy application ```fly deploy```
4. Create super user by connecting to app with ```flyctl ssh console``` and ```createsuperuser``` and ```collectstatic``` to cloudinary using manage.py

