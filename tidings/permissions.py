from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated
from .models import Comment
from django.utils import timezone
from profile.permissions import UnblockedPermission
from django.core.exceptions import ObjectDoesNotExist

class CommentPermission(UnblockedPermission):
    
    messageTime = 'Too many comments. Wait 5 minutes'
    messageBlocked = 'Blocked profile is not allowed to comment'
    
    MIN_TIME = 300
    
    def has_permission(self, request, view):
        if(not super().has_permission(request, view)):
            self.message = self.messageBlocked
            return False
        self.message = self.messageTime
        try:
            lastCommentDate = Comment.objects.filter(user = request.user).latest('created').created
            duration = timezone.now() - lastCommentDate
            return duration.total_seconds() > self.MIN_TIME
        except ObjectDoesNotExist:
            return True
    