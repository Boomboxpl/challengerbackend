from django.db.models.signals import post_save
from .models import AdminMessage, News, HighScore, Tiding
from django.dispatch import receiver

@receiver(post_save, sender=News)
@receiver(post_save, sender=HighScore)
@receiver(post_save, sender=AdminMessage)
def create_tiding(sender, instance, created, **kwargs):
    if created:
        instance.tiding = Tiding.objects.create(created = instance.created)

@receiver(post_save, sender=News)
@receiver(post_save, sender=HighScore)
@receiver(post_save, sender=AdminMessage)
def save_tiding(sender, instance, **kwargs):
    instance.tiding.save()