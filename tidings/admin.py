from django.contrib import admin
from .models import AdminMessage, Comment, Tiding, News

# Register your models here.
admin.site.register(Comment)
admin.site.register(Tiding)
admin.site.register(News)
admin.site.register(AdminMessage)