from rest_framework import serializers
from .models import AdminMessage, News, Tiding, Comment, Game, HighScore, User
from highscores.serializers import HighScoreFullSerializer

class NewsSerializer(serializers.ModelSerializer):
    game = serializers.PrimaryKeyRelatedField(queryset = Game.objects.all())
    tiding = serializers.PrimaryKeyRelatedField(read_only = True)
    
    class Meta:
        model = News
        fields = '__all__'
        
class AdminMessageSerializer(serializers.ModelSerializer):
    allowComment = serializers.BooleanField(default=True, write_only=True)
    tiding = serializers.PrimaryKeyRelatedField(read_only = True)
    
    def create(self, validated_data):
        allow = validated_data.pop('allowComment')
        obj = AdminMessage.objects.create(**validated_data)
        obj.tiding.allowComment = allow
        obj.save()
        return obj
    
    def update(self, instance, validated_data):
        instance.tiding.allowComment = validated_data.get('allowComment', instance.tiding.allowComment)
        instance.save()
        return instance
    
    class Meta:
        model = AdminMessage
        fields = '__all__'

class TidingSerializer (serializers.ModelSerializer):
    
    class Meta:
        model = Tiding
        fields = '__all__'
        
class TidingSerializerUserUpdate (serializers.ModelSerializer):
    
    class Meta:
        model = Tiding
        fields = ['allowComment']
        
class CommentCreateSerializer (serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    
    class Meta:
        model = Comment
        fields = ['id','tiding', 'user', 'content']
        read_only_fields = ['id']
        
class CommentManageSerializer (CommentCreateSerializer):

    class Meta:
        model = Comment
        fields = '__all__'
        
class TidingFullSerializer (serializers.ModelSerializer):
    highscore = HighScoreFullSerializer()
    news = NewsSerializer()
    message = AdminMessageSerializer()
    comment_set = CommentManageSerializer(many = True)
    
    class Meta:
        model = Tiding
        fields = '__all__'
        