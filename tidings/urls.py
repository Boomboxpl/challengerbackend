from django.urls import include, path
from .views import *

urlpatterns = [
    path('', TidingsList.as_view()),
    path('messages', MessagesList.as_view()),
    path('news', NewsList.as_view()),
    path('full', TidingFullList.as_view()),
    path('<int:pk>/', TidingDetail.as_view()),
    path('update/<int:pk>/', TidingUpdateAdmin.as_view()),
    path('comment/create/', CommentCreate.as_view()),
    path('comment/<int:pk>/', CommentManage.as_view()),
    path('update/own/<int:pk>/', TidingUpdateUser.as_view()),
    path('adminmessage/', AdminMessageCreate.as_view()),
    path('adminmessage/<int:pk>', AdminMessageManage.as_view())
]
