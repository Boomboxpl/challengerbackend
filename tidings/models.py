from django.db import models
from highscores.models import HighScore
from games.models import Game
from django.contrib.auth.models import User

# Create your models here.
    
class News(models.Model):
    game = models.ForeignKey(Game, on_delete=models.SET_NULL, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    isActive = models.BooleanField(default=True)
    dealID = models.CharField(max_length=100, null=True, blank=True)
    dealRating = models.FloatField(default = 0)
    
    def __str__(self):
        if self.isActive:
            return f"Active {self.dealID}"
        return f"Inactive {self.dealID}"
    
class AdminMessage(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    isArchived = models.BooleanField(default=False, blank=True)
    content = models.CharField(max_length=60000)
    
    def __str__(self):
        if not self.isArchived:
            return f"Admin Message" + self.created.strftime("%m/%d/%Y, %H:%M:%S")
        return f"Archived Message" + self.created.strftime("%m/%d/%Y, %H:%M:%S")

class Tiding(models.Model):
    created = models.DateTimeField(auto_now_add=False)
    isBlocked = models.BooleanField(default = False)
    highscore = models.OneToOneField(HighScore, on_delete=models.CASCADE, null= True, blank= True)
    news = models.OneToOneField(News, on_delete=models.CASCADE, null= True, blank= True)
    message = models.OneToOneField(AdminMessage, on_delete=models.CASCADE, null= True, blank= True)
    allowComment = models.BooleanField(default=True, blank=True)
    
    @property
    def user(self):
        if self.highscore is None:
            return None
        return self.highscore.user
    
    def __str__(self):
        if self.highscore is not None:
            return self.highscore.name
        if self.news is not None:
            return "Sale " + self.created.strftime("%m/%d/%Y, %H:%M:%S")
        if self.message is not None:
            return self.message.content[0:20]
        return "Undefined"
    
class Comment(models.Model):
    tiding = models.ForeignKey(Tiding, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null= True, blank= True)
    created = models.DateTimeField(auto_now_add=True)
    isBlocked = models.BooleanField(default = False)
    content = models.CharField(max_length=2000)
    
    def __str__(self):
        return self.content[0:50]