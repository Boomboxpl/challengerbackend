# Generated by Django 4.1.3 on 2022-12-29 14:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0005_alter_game_user'),
        ('tidings', '0002_rename_author_comment_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='dealID',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='news',
            name='dealRating',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='news',
            name='isActive',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='news',
            name='game',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='games.game'),
        ),
    ]
