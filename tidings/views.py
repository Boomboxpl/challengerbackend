from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import IsAdminUser
from challenger.pagination import CursorCreatedPagination
from challenger.permissions import OwnerPermission, IsAuthenticated
from .models import AdminMessage, Comment, News, Tiding
from .serializers import (AdminMessageSerializer, CommentCreateSerializer, CommentManageSerializer,
                          NewsSerializer, TidingFullSerializer,
                          TidingSerializer, TidingSerializerUserUpdate)
from .permissions import CommentPermission
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


# Create your views here.
class TidingsList(generics.ListAPIView):
    queryset = Tiding.objects.all()
    serializer_class = TidingSerializer
    pagination_class = CursorCreatedPagination
    
    filterset_fields = {
        'created' : ['range'],
        'highscore' : ['isnull'],
        'message' : ['isnull'],
        'news' : ['isnull']
    }
    ordering_fields = '__all__'
    ordering = ['-created']
    
    @method_decorator(cache_page(360*5))
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
   
class NewsList(generics.ListAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    pagination_class = CursorCreatedPagination
    
    filterset_fields = {
        'created' : ['range'],
        'isActive' : ['exact'],
        'dealRating' : ['exact', 'gt', 'lt'],
    }
    ordering_fields = '__all__'
    ordering = ['-created']
    
    @method_decorator(cache_page(60*5))
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    
class MessagesList(generics.ListAPIView):
    queryset = AdminMessage.objects.all()
    serializer_class = AdminMessageSerializer
    pagination_class = CursorCreatedPagination
    
    filterset_fields = {
        'created' : ['range'],
        'content' : ['exact', 'contains'],
        'isArchived' : ['exact']
    }
    ordering_fields = '__all__'
    ordering = ['-created']
    
    @method_decorator(cache_page(60*30))
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    
class TidingFullList(generics.ListAPIView):
    queryset = Tiding.objects.all()
    serializer_class = TidingFullSerializer
    pagination_class = CursorCreatedPagination
    
    filterset_fields = {
        'created' : ['range'],
        'highscore' : ['isnull'],
        'message' : ['isnull'],
        'news' : ['isnull']
    }
    ordering_fields = '__all__'
    ordering = ['-created']
    
class TidingDetail(generics.RetrieveAPIView):
    queryset = Tiding.objects.all()
    serializer_class = TidingFullSerializer
    
class TidingUpdateAdmin(generics.UpdateAPIView):
    permission_classes = [IsAdminUser]
    
    queryset = Tiding.objects.all()
    serializer_class = TidingFullSerializer
    
class TidingUpdateUser(generics.UpdateAPIView):
    permission_classes = [OwnerPermission]
    
    queryset = Tiding.objects.all()
    serializer_class = TidingSerializerUserUpdate

class AdminMessageCreate(generics.CreateAPIView):
    permission_classes = [IsAdminUser]
    queryset = Comment.objects.all()
    serializer_class = AdminMessageSerializer

class AdminMessageManage(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAdminUser]
    queryset = Comment.objects.all()
    serializer_class = AdminMessageSerializer
    
class CommentCreate(generics.CreateAPIView):
    queryset = Comment.objects.all()
    permission_classes = (CommentPermission, )
    serializer_class = CommentCreateSerializer
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    
class CommentsList(generics.ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentManageSerializer
    pagination_class = CursorCreatedPagination
    
    filterset_fields = '__all__'
    ordering = ['-created']
    
class CommentManage(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [(IsAuthenticated & OwnerPermission) | IsAdminUser]
    queryset = Comment.objects.all()
    
    def get_serializer_class(self):
        if self.request.user.is_staff:
            return CommentManageSerializer
        return CommentCreateSerializer
     
    