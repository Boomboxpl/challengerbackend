import datetime
import requests
import json
from types import SimpleNamespace
from .models import News
from games.models import Game
from django.db.models import Q
from challenger.settings import CHEAPSHARK_URL
from django.utils import timezone

def tryBindGame(response):
    try:
        return Game.objects.filter(name = response.title).first()
    except Game.DoesNotExist:
        return None

def refreshNews():
    newDeals = getNewDeals(25)
    deactivateOld(newDeals)
    payload = {}
    headers = {}
    try:
        response = newDeals[0:59]
        for gameResponse in response:
            if not News.objects.filter(dealID = gameResponse.dealID).exists():
                foundGame = tryBindGame(gameResponse)
                news = News(
                    game = foundGame,
                    isActive = True,
                    dealID = gameResponse.dealID, 
                    dealRating = gameResponse.dealRating)
                news.save()
    except TypeError:
        print(f"Error for CheapSharkAPI updating news")
        
        
def deactivateOld(newDeals):
    dealIds = [deal.dealID for deal in newDeals]
    monthAgo = timezone.now() - datetime.timedelta(days=31)
    isActive = Q(dealID__in = dealIds) | Q(created__gt = monthAgo)
    News.objects.filter(isActive = True).exclude(isActive).update(isActive = False)

def getNewDeals(pageCount):
    response = []
    for page in range(0, pageCount):
        responseJson = requests.request("GET", CHEAPSHARK_URL)
        try:
            iterResponse = json.loads(responseJson.content, object_hook=lambda d: SimpleNamespace(**d))
            try:
                some_object_iterator = iter(iterResponse)
                response += iterResponse
            except TypeError as te:
                break
        except TypeError:
            print(f"Error for CheapSharkAPI: {responseJson.status_code.__str__()} {responseJson.text}")
    return response
        
        