from django.apps import AppConfig


class TidingsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tidings'
    
    def ready(self):
        import tidings.signals
