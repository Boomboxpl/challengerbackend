from django.db import models
from django.contrib.auth.models import User
import uuid

GAME_API_LENGHT = 20

# Create your models here.

def getRandomApi():
    return uuid.uuid4().hex[:(GAME_API_LENGHT)].upper()

class Game(models.Model):
    name = models.CharField("Name", max_length=240)
    description = models.CharField("Description", max_length=240, blank= True, default= "")
    metaData = models.CharField("Meta", max_length=100, blank= True, default= "")
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank= True)
    created = models.DateTimeField(auto_now_add=True)
    releaseDate =  models.DateField(null= True, blank=True)
    apiKey = models.CharField(max_length=GAME_API_LENGHT, default = getRandomApi)

    def __str__(self):
        return self.name
    