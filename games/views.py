import json
from django.shortcuts import render
import requests
from rest_framework import generics
from .models import Game
from .serializers import GameCreateSerializer, GameSerializer, GameDeveloperSerializer
from profile.permissions import DeveloperPermission
from challenger.permissions import OwnerPermission
from rest_framework.views import APIView
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from drf_spectacular.utils import OpenApiParameter, OpenApiResponse, extend_schema, extend_schema_field
import games.igdb
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.permissions import IsAdminUser

# Create your views here.
class GameCreate(generics.CreateAPIView):
    queryset = Game.objects.all()
    permission_classes = [DeveloperPermission]
    serializer_class = GameCreateSerializer
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class GameList(generics.ListAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    ordering = ['-created']
    
    @method_decorator(cache_page(30))
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    
class GameDetail(generics.RetrieveAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    
class GameListDeveloper(generics.ListAPIView):
    def get_queryset(self):
        return Game.objects.filter(user = self.request.user)
    
    permission_classes = [DeveloperPermission, OwnerPermission]
    serializer_class = GameDeveloperSerializer
    ordering = ['-created']
    
class GameUpdate(generics.RetrieveUpdateAPIView):
    queryset = Game.objects.all()
    permission_classes = [(DeveloperPermission & OwnerPermission) | IsAdminUser]
    serializer_class = GameDeveloperSerializer
    
class GameDelete(generics.RetrieveDestroyAPIView):
    queryset = Game.objects.all()
    permission_classes = [(DeveloperPermission & OwnerPermission) | IsAdminUser]
    serializer_class = GameDeveloperSerializer
    
#IGDB database

class IGDBUnavailable(APIException):
    status_code = 503
    default_detail = 'IGDB temporarily unavailable, try again later.'
    default_code = 'service_unavailable'

@extend_schema(responses=
            {200:OpenApiResponse(
                description='success',
            ), 503:OpenApiResponse(
                description='service_unavailable',
            )}, description="Check request & responses on https://api-docs.igdb.com/#examples")
class GameDatabase(APIView):
    @method_decorator(cache_page(60*60*24))
    def get(self, request, query):
        try:
            games.igdb.tryInit()
            response = games.igdb.igdb_wrapper.api_request('games', query)
            return Response(response.json())
        except requests.HTTPError:
            raise IGDBUnavailable()
