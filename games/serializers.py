from rest_framework import serializers
from .models import Game

class GameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game 
        exclude = ['apiKey']
        
class GameDeveloperSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game 
        fields = '__all__'
        
class GameCreateSerializer(GameDeveloperSerializer):
    
    class Meta:
        model = Game
        fields = '__all__'
        read_only_fields = ['user', 'apiKey']