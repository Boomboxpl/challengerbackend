from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated
from .models import Game
from django.core.exceptions import ObjectDoesNotExist

class GameApiPermission(permissions.BasePermission):
    message = "Invalid API Key. You probably not playing newest game version."
    
    def checkGamePermission(self, game, request):
        try:
            apiKey = request.headers.get('GAME_API')
            if callable(game):
                game = game()
            if apiKey:
                return game.apiKey == apiKey
            else:
                return False
        except ObjectDoesNotExist:
            return False
    
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        gameid = request.data['game']
        return self.checkGamePermission(lambda: Game.objects.get(id = gameid), request)
    
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return self.checkGamePermission(obj, request)