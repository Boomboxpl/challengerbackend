from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Game, getRandomApi

@receiver(post_save, sender=Game)
def create_game(sender, instance, created, **kwargs):
    if created:
        instance.apiKey = getRandomApi()
    
