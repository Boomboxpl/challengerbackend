import json
import os
from types import SimpleNamespace
from igdb.wrapper import IGDBWrapper
import requests

igdb_wrapper : IGDBWrapper = None

def tryInit():
    global igdb_wrapper
    if igdb_wrapper is None:
        refreshToken()

def api_request(self, endpoint:str, query:str) -> requests.Response:
        """
        Fixed IGDBWrapper function
        """
        global igdb_wrapper
        tryInit()
        url = IGDBWrapper._build_url(endpoint)
        params = self._compose_request(query)

        response : requests.Response = requests.post(url, **params)
        response.raise_for_status()

        return response

def refreshToken():
    global igdb_wrapper
    IGDBWrapper.api_request = api_request
    id = os.environ.get('IGDB_ID', "")
    secret = os.environ.get('IGDB_PASSWORD', "")
    responseJson = requests.post(f"https://id.twitch.tv/oauth2/token?client_id={id}&client_secret={secret}&grant_type=client_credentials")
    try:
        response = json.loads(responseJson.content, object_hook=lambda d: SimpleNamespace(**d))
        igdb_wrapper = IGDBWrapper(id, response.access_token)
    except TypeError:
        print(f"Error for IGDB Oauth: {responseJson.status_code.__str__()} {responseJson.text}")
        