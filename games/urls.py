from django.urls import include, path
from .views import GameCreate, GameList, GameDetail, GameListDeveloper, GameUpdate, GameDelete, GameDatabase


urlpatterns = [
    path('create/', GameCreate.as_view(), name='create-game'),
    path('', GameList.as_view()),
    path('<int:pk>/', GameDetail.as_view(), name='retrieve-game'),
    path('managed/', GameListDeveloper.as_view()),
    path('update/<int:pk>/', GameUpdate.as_view(), name='update-game'),
    path('delete/<int:pk>/', GameDelete.as_view(), name='delete-game'),
    path('igdb/<str:query>', GameDatabase.as_view())
]