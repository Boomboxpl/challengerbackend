# Generated by Django 4.1.3 on 2022-11-26 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0003_game_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='description',
            field=models.CharField(blank=True, default='', max_length=240, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='game',
            name='metaData',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Meta'),
        ),
    ]
