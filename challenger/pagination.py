from rest_framework.pagination import CursorPagination

class CursorKeyPagination(CursorPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    cursor_query_param = 'c'
    ordering = 'pk'
    
class CursorCreatedPagination(CursorKeyPagination):
    ordering = '-created'