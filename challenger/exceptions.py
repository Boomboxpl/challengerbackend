from rest_framework.views import exception_handler

def axios_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None and response.status_code == 400:
        response.data['status_code'] = response.status_code
        response.status_code = 207

    return response