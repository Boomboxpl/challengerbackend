from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated

class OwnerPermission(permissions.BasePermission):
    message = "You're not owner"
    
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return request.user.is_authenticated and obj.user == request.user